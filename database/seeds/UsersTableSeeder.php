<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'admin',
            'email' => 'admin@gmail.com',
            'password' => bcrypt('adminsecret'),
        ]);
        DB::table('users')->insert([
            'name' => 'test1',
            'email' => 'test1@gmail.com',
            'password' => bcrypt('test1secret'),
        ]);
        DB::table('users')->insert([
            'name' => 'test2',
            'email' => 'test2@gmail.com',
            'password' => bcrypt('test2secret'),
        ]);
        DB::table('users')->insert([
            'name' => 'test3',
            'email' => 'test3@gmail.com',
            'password' => bcrypt('test3secret'),
        ]);
        DB::table('users')->insert([
            'name' => 'test4',
            'email' => 'test4@gmail.com',
            'password' => bcrypt('test4secret'),
        ]);
        DB::table('users')->insert([
            'name' => 'test5',
            'email' => 'test5@gmail.com',
            'password' => bcrypt('test5secret'),
        ]);
        DB::table('users')->insert([
            'name' => 'test6',
            'email' => 'test6@gmail.com',
            'password' => bcrypt('test6secret'),
        ]);
    }
}
