<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVideoOptionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('video_option', function (Blueprint $table) {
            $table->unsignedInteger('video_id')->primary();
            $table->foreign('video_id')->references('id')->on('videos');

            $table->boolean('is_send');
            $table->boolean('is_moderation');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('video_option');
    }
}
