$(document).ready(() => {
    $('[js-filed-upload-video]').on('change', (e) => {
        let files = e.currentTarget.files;
        let currentField = $(e.currentTarget);

        currentField.addClass('custom-file-input');

        let checkSize = checkSizeFile(files[0]);
        let checkType = checkTypeFile(files[0]);

        $('[js-field-filename]', '[js-form-upload]').text(files[0].name);

        currentField.removeClass('is-valid');
        currentField.removeClass('is-invalid');

        if (checkSize && checkType) {
            currentField.addClass('is-valid');
            $('[js-btn-submit]', '[js-form-upload]').removeAttr('disabled');
        } else {
            currentField.addClass('is-invalid');
            $('[js-btn-submit]', '[js-form-upload]').attr('disabled', 'disabled');
        }
    });

    $('#videos').on('click','[js-btn-allow-video]', (e) => {
        let id = $(e.target).data('id');

        actionVideos('allow', id);
    });

    $('#videos').on('click', '[js-btn-remove-video]', (e) => {
        let id = $(e.target).data('id');
        actionVideos('remove', id);
    });
});

let checkSizeFile = (file) => {
    let fileSize = file.size/1000;

    return fileSize <= 500000;
};

let checkTypeFile = (file) => {
    let fileType = file.type;

    return fileType.indexOf('video/') !== -1;
};

let reloadPostData = (block) => {
    var self = this;
    var url = window.location.search;

    $.ajax({
        url: url,
        type: "get",
        datatype: "html",
    }).done(function (data) {
        $(block).empty().html(data);
    });
};

let actionVideos = (action, id) => {
    $.ajax({
        url: '/video/' + action + '/' + id,
        method: 'POST',
        type: 'json',
        success: (data) => {
            if(data.code) {
                reloadPostData('#videos');
            }
        },
    });
};