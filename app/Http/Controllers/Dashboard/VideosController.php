<?php

namespace App\Http\Controllers\Dashboard;

use App\Dashboard\Videos;
use FFMpeg\Media\Video;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class VideosController extends Controller
{
    public function videos(Request $request)
    {
        $offset = $request->get('offset', 0);
        $limit = $request->get('limit', 10);
        $videos = Videos::offset($offset)->limit($limit)->get();

        $videos = $videos->map(function ($video) {
            return [
                $video['id'] => url(Storage::url($video['file_name']))
            ];
        });

        return $videos;
    }

    public function remove(Request $request)
    {
        $id = $request->get('id');
        $video = Videos::find($id);
        $result = [
            'code' => false,
        ];

        if (!$video) {
            return $result;
        }

        $result['code'] = Storage::delete('public/' . $video->file_name);
        $video->option->is_send = true;

        $result['code'] &= $video->option->save();

        return $result;
    }
}
