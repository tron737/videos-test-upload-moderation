<?php

namespace App\Http\Controllers\Dashboard;

use App\Dashboard\VideoOption;
use App\Dashboard\Videos;
use App\Dashboard\VideoUser;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class IndexController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $userId = Auth::user()->getAuthIdentifier();
        $currentDate = new \DateTime();
        $currentDate->modify('-7 day');
        $video = Videos::leftJoin('video_user', 'video_user.video_id', '=', 'id')->where('video_user.user_id', '=', $userId)->where('created_at', '>=', $currentDate->format('Y-m-d H:i:s'))->get();
        return view('dashboard.upload', [
            'cntUploadVideo' => $video->count(),
        ]);
    }

    public function upload(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'video' => 'required|file|max:500000',
        ]);

        if ($validator->failed()) {
            return redirect('/');
        }

        $fileName = time() . '_' . $request->file('video')->getClientOriginalName();

        $video = new Videos();
        $video->original_file_name = $request->file('video')->getClientOriginalName();
        $video->file_name = $fileName;
        $video->save();

        $video->user()->save(new VideoUser());
        $video->option()->save(new VideoOption());

        $request->video->storeAs('public', $fileName);

        return redirect('/');
    }

    public function videos(Request $request)
    {
        $moderation = $request->get('moderation', null);
        $sending = $request->get('sending', null);

        $videos = Videos::with(['user', 'option'])->leftJoin('video_option', 'video_option.video_id', '=', 'id');

        if ($moderation) {
            $videos = $videos->where('is_moderation', true);
        }

        if ($sending) {
            $videos = $videos->where('is_send', true);
        }

        $videos = $videos->paginate(10);

        $params = [
            'videos' => $videos,
            'moderation' => $moderation,
            'sending' => $sending,
        ];

        if ($request->ajax()) {
            return view('dashboard.videos.table', $params);
        } else {
            return view('dashboard.videos.index', $params);
        }
    }

    public function video(Videos $video)
    {
        return view('dashboard.video', [
            'videoUrl' => Storage::url($video->file_name)
        ]);
    }

    public function allow(Videos $video)
    {
        $result = [
            'code' => false,
        ];
        if (!$video->option->is_moderation) {
            $video->option->is_moderation = true;
            $result['code'] = $video->option->save();
        }

        return $result;
    }

    public function remove(Videos $video)
    {
        $result = [
            'code' => false,
        ];

        $result['code'] = Storage::delete('public/' . $video->file_name);

        $result['code'] &= $video->user->delete() && $video->option->delete() && $video->delete();

        return $result;
    }

//    public function test()
//    {
//        $client = new Client();
//        $res = $client->get(config('app.public_server_url') . '/api/videos');
//        $videos = json_decode($res->getBody());
//        foreach ($videos as $video) {
//            foreach ($video as $videoId => $videoUrl) {
//                if (DB::table('jobs')->where('queue', 'default')->count() < 2) {
//                    $job = (new ConvertVideo($videoUrl, $videoId));
//                    dispatch($job);
//                }
//            }
//        }
//    }
}
