<?php

namespace App\Http\Controllers\Dashboard;

use App\Dashboard\UserRole;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;

class UsersController extends Controller
{
    public function users()
    {
        $users = User::paginate(10);
        return view('dashboard.users.index', [
            'users' => $users,
        ]);
    }

    public function add()
    {
        return view('dashboard.users.add');
    }

    public function adduser(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
            'role' => ['required', 'integer', 'min:1', 'max:2'],
        ]);

        if ($validator->failed()) {
            return redirect('/users');
        }

        $user = new User();
        $user->name = $request->get('name');
        $user->email = $request->get('email');
        $user->password = Hash::make($request->get('password'));
        $user->save();

        $user->role()->save(new UserRole(['role_id' => $request->get('role')]));

        return redirect('/users');
    }
}
