<?php

namespace App\Console;

use App\Dashboard\UserRole;
use App\Mail\Videos;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use App\Jobs\ConvertVideo;
use GuzzleHttp\Client;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        //отправка писем на почту
        if (config('app.is_public_server')) {
            $schedule->call(function () {
                $currentDate = new \DateTime();
                $currentDate->modify('-3 day');
                $videos = Videos::leftJoin('video_option', 'video_option.video_id', '=', 'id')->where('video_option.is_moderation', false)->where('created_at', '<=', $currentDate->format('Y-m-d H:i:s'))->get();

                $users = UserRole::with(['user'])->where('role_id', '=', 1)->get();

                foreach ($users as $user) {
                    Mail::to($user->user)->send(new Videos($videos));
                }
            })->dailyAt('12:00');
        } else {
            //добавление задания в очередь
            $schedule->call(function () {
                $client = new Client();
                $res = $client->get(config('app.public_server_url') . '/api/videos');
                $videos = json_decode($res->getBody());
                foreach ($videos as $video) {
                    foreach ($video as $videoId => $videoUrl) {
                        if (DB::table('jobs')->where('queue', 'default')->count() < 2) {
                            $job = (new ConvertVideo($videoUrl, $videoId));
                            dispatch($job);
                        }
                    }
                }
            })->everyMinute();
        }
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
