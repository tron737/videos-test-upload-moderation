<?php

namespace App\Dashboard;

use Illuminate\Database\Eloquent\Model;

class Videos extends Model
{
    protected $table = 'videos';

    public function user()
    {
        return $this->hasOne('App\Dashboard\VideoUser', 'video_id', 'id');
    }

    public function option()
    {
        return $this->hasOne('App\Dashboard\VideoOption', 'video_id', 'id');
    }
}
