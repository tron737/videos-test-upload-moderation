<?php

namespace App\Dashboard;

use Illuminate\Database\Eloquent\Model;

class UserRole extends Model
{
    protected $table = 'user_role';
    public $timestamps = false;
    protected $primaryKey = 'user_id';

    protected $fillable = ['role_id'];

    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }
}
