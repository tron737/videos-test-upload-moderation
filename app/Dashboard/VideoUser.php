<?php

namespace App\Dashboard;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class VideoUser extends Model
{
    protected $table = 'video_user';
    protected $primaryKey = 'video_id';
    public $timestamps = false;

    protected $fillable = ['user_id', 'original_file_name', 'file_name'];

    public function __construct(array $attributes = [])
    {
        if (!isset($attributes['user_id'])) {
            $attributes['user_id'] = Auth::user()->getAuthIdentifier();
        }
        parent::__construct($attributes);
    }
}
