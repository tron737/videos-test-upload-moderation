<?php

namespace App\Dashboard;

use Illuminate\Database\Eloquent\Model;

class VideoOption extends Model
{
    protected $table = 'video_option';
    protected $primaryKey = 'video_id';
    public $timestamps = false;

    protected $attributes = [
        'is_send' => false,
        'is_moderation' => false,
    ];
}
