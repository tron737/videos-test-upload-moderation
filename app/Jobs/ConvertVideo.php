<?php

namespace App\Jobs;

use GuzzleHttp\Client;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ConvertVideo implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $videoUrl = null;
    protected $videoId = null;

    /**
     * Create a new job instance.
     *
     * @param string $videoUrl
     * @param int $videoId
     * @return void
     */
    public function __construct($videoUrl, $videoId)
    {
        $this->videoUrl = $videoUrl;
        $this->videoId = $videoId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $fileName = time();
        exec('wget -O ' . $fileName . ' "http://videotest/storage/1549816997_Работа скрипта по определению профиля вк.mov"');
        exec('ffmpeg -i ' . $fileName . ' -codec copy ' . $fileName . '.mp4');
        $client = new Client();
        $params['form_params'] = [
            'id' => $this->videoId,
        ];
        $response = $client->post(config('app.public_server_url') . '/api/video/remove', $params);

        $response = json_decode($response->getBody());
        if ($response->code) {
            exec('rm -rf ' . $fileName);
        }
    }
}
