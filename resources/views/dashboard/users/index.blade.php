@extends('layouts.app')

@section('content')
    <div class="container">
        <a href="{{ url('user/add') }}" class="btn btn-outline-success">Добавить пользователя</a>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Email</th>
                <th scope="col">Имя</th>
                <th scope="col">Роль</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($users as $user)
                <tr>
                    <th scope="row">{{ $user->id }}</th>
                    <td>{{ $user->email }}</td>
                    <td>{{ $user->name }}</td>
                    <td>{{ $user->role->role_id == 1 ? 'администратор' : 'пользователь' }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{ $users->links() }}
    </div>
@endsection