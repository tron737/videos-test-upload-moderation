@extends('layouts.app')

@section('content')
    <div class="container">
        @if($cntUploadVideo == 0)
            <form method="POST" class="form-inline" action="{{ url('/upload') }}" enctype="multipart/form-data"
                  js-form-upload>
                @csrf
                <div class="form-group w-75">
                    <div class="custom-file">
                        <input type="file" id="upload_file" name="video" accept="video/*" required
                               js-filed-upload-video>
                        <label class="custom-file-label" for="upload_file"><span js-field-filename></span></label>
                        <div class="invalid-feedback">Ошибка формата или веса файла(макс. 500MB)</div>
                    </div>
                </div>

                <button class="btn btn-primary ml-3" type="submit" disabled js-btn-submit>Загрузить</button>
            </form>
        @else
            Загрузка видео доступна раз в 7 дней
        @endif
    </div>
@endsection