<table class="table">
    <thead>
    <tr>
        <th scope="col">#</th>
        <th scope="col">Файл</th>
        <th scope="col">Дата загрузки</th>
        <th scope="col">Статус модерации</th>
        <th scope="col">Статус отправки</th>
        <th scope="col"></th>
    </tr>
    </thead>
    <tbody>
    @foreach($videos as $video)
        @php
        $currentDate = new DateTime();
        $createdVideoDate = new DateTime($video->created_at);
        $diffDay = $createdVideoDate->diff($currentDate)->d;
        @endphp
        <tr>
            <th scope="row">{{ $video->id }}</th>
            <td>
                @if ($video->option->is_send == 0)
                    <a href="{{ url('video/' . $video->id) }}">{{ $video->original_file_name }}</a>
                @else
                    {{ $video->original_file_name }}
                @endif
            </td>
            <td>{{ $video->created_at }}</td>
            <td>{!! $video->option->is_moderation == 0 ? '<span class="badge badge-danger">на модерации</span>' : '<span class="badge badge-success">отмодерировано</span>' !!}</td>
            <td>{!! $video->option->is_send == 0 ? '<span class="badge badge-danger">ожидает отправки</span>' : '<span class="badge badge-success">отправлено</span>' !!}</td>
            <td>
                @if ($video->option->is_moderation == 0 && $diffDay < 3)
                    <a href="javascript:;" class="btn btn-success" data-id="{{ $video->id }}" js-btn-allow-video>Отмодерировано</a>
                    <a href="javascript:;" class="btn btn-danger" data-id="{{ $video->id }}"
                       js-btn-remove-video>Удалить</a>
                @endif
            </td>
        </tr>
    @endforeach
    </tbody>
</table>

{{ $videos->links() }}