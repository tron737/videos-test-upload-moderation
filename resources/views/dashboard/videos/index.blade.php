@extends('layouts.app')

@section('content')
    <div class="container">
        <form>
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="checkbox" id="moder" name="moderation" value="1" {{ $moderation ? 'checked' : '' }}>
                <label class="form-check-label" for="moder">Отмодерировано</label>
            </div>
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="checkbox" id="sending" name="sending" value="1" {{ $sending ? 'checked' : '' }}>
                <label class="form-check-label" for="sending">Отправлено</label>
            </div>
            <button type="submit" class="btn btn-outline-success">Применить</button>
        </form>
        <div id="videos">
            @include('dashboard.videos.table')
        </div>
    </div>
@endsection