@extends('layouts.app')

@section('content')
    <div class="container">
        <video width="700" controls="controls">
            <source src="{{ url($videoUrl) }}">
        </video>
        <div>
            <a href="{{ URL::previous() == URL::current() ? url('/videos') : url(URL::previous()) }}">Назад</a>
        </div>
    </div>
@endsection