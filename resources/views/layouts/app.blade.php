<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v3.8.5">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Bootstrap core CSS -->
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">


    <!-- Custom styles for this template -->
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
</head>
<body>
<div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom shadow-sm">
    <h5 class="my-0 mr-md-auto font-weight-normal"><a href="{{ url('/') }}" class="">{{ config('app.name', 'Laravel') }}</a></h5>
    @guest
        <a class="btn btn-outline-primary" href="{{ route('login') }}">Авторизация</a>
    @else
        @if(\Illuminate\Support\Facades\Auth::user()->role->role_id == 1)
        <a class="p-2 {{ Request::is('videos') ? 'text-dark' : 'text-primary' }}" href="{{ url('/videos') }}">Все видео</a>
        <a class="p-2 {{ Request::is('users') ? 'text-dark' : 'text-primary' }}" href="{{ url('/users') }}">Пользователи</a>
        @endif
        <a class="p-2 {{ Request::is('/') ? 'text-dark' : 'text-primary' }}" href="{{ url('/') }}">Загрузить видео</a>
        <a class="btn btn-outline-danger" href="{{ route('logout') }}"
           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
            Выход
        </a>

        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
        </form>
    @endguest
</div>

<div class="container">
    @yield('content')
</div>

<script src="{{ asset('js/jquery-3.3.1.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/bootstrap.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/script.js') }}" type="text/javascript"></script>
</body>
</html>
