<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

if (config('app.is_public_server')) {
    Auth::routes();

    Route::get('/', 'Dashboard\IndexController@index');

    Route::post('/upload', 'Dashboard\IndexController@upload');

    Route::get('/videos', 'Dashboard\IndexController@videos');
    Route::get('/video/{video}', 'Dashboard\IndexController@video');
    Route::post('/video/allow/{video}', 'Dashboard\IndexController@allow');
    Route::post('/video/remove/{video}', 'Dashboard\IndexController@remove');

    Route::get('/users', 'Dashboard\UsersController@users');
    Route::get('/user/add', 'Dashboard\UsersController@add');
    Route::post('/user/add', 'Dashboard\UsersController@adduser');
}